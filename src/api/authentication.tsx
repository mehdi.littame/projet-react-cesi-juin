import {
  createUserWithEmailAndPassword,
  signInWithEmailAndPassword,
  signOut,
  sendPasswordResetEmail,
  sendEmailVerification,
} from "firebase/auth";
import { auth } from "@/config/firebase-config";
import { FirebaseError } from "firebase/app";

export const firebaseCreateUser = async (email: string, password: string) => {
  try {
    const userCredential = await createUserWithEmailAndPassword(
      auth,
      email,
      password
    ); //tant que t'as pas fait la fonction tu patientes et ensuite tu transmets le resultat à usercredential
    return { data: userCredential.user }; //en cas de succes je return un objet qui contient des données
  } catch (error) {
    const firebaseError = error as FirebaseError; //typage de firebase
    return {
      error: {
        code: firebaseError.code,
        message: firebaseError.message,
      },
    };
  }
};

export const firebaseSignInUser = async (email: string, password: string) => {
  try {
    const userCredential = await signInWithEmailAndPassword(
      auth,
      email,
      password
    ); //tant que t'as pas fait la fonction tu patientes et ensuite tu transmets le resultat à usercredential
    return { data: userCredential.user }; //en cas de succes je return un objet qui contient des données
  } catch (error) {
    const firebaseError = error as FirebaseError; //typage de firebase
    return {
      error: {
        code: firebaseError.code,
        message: firebaseError.message,
      },
    };
  }
};

export const firebaseLogoutUser = async () => {
  try {
    await signOut(auth);
    return { data: true }; //
  } catch (error) {
    const firebaseError = error as FirebaseError; //typage de firebase
    return {
      error: {
        code: firebaseError.code,
        message: firebaseError.message,
      },
    };
  }
};

export const sendEmailToResetPassword = async (email: string) => {
  try {
    await sendPasswordResetEmail(auth, email);
    return { data: true }; //on attend pas d'infos
  } catch (error) {
    const firebaseError = error as FirebaseError; //typage de firebase
    return {
      error: {
        code: firebaseError.code,
        message: firebaseError.message,
      },
    };
  }
};

//envoi mail de confirmation
export const sendEmailToConfirm = async () => {
  if (auth.currentUser) {
    try {
      await sendEmailVerification(auth.currentUser);
      return { data: true };
    } catch (error) {
      const firebaseError = error as FirebaseError;
      return {
        error: {
          code: firebaseError.code,
          message: firebaseError.message,
        },
      };
    }
  } else {
    return {
      error: {
        code: "unknow",
        message: "Une erreur est survenue",
      },
    };
  }
};
