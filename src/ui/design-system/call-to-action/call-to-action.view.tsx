import { Container } from "@/ui/components/container/container";
import { Button } from "@/ui/design-system/button/button";
import { Typography } from "../typography/typography";
import { LinkTypes } from "@/lib/link-type";
import Image from "next/image";

export const CallToActionView = () => {
  return (
    <div className="relative overflow-hidden bg-primary">
      <Container className="py-20">
        <div className="relative z-10 max-w-3xl space-y-5">
          <Typography variant="h2" theme="white" component="h2">
            N’attend pas pour développer tes compétences...
          </Typography>
          <div>
            <Button variant="white" baseUrl="#" linkType={LinkTypes.EXTERNAL}>
              Formation dev
            </Button>
          </div>
        </div>
        <div className="">
          <Image
            src="/assets/svg/bomb.svg"
            alt="image d'une bombe"
            width={1410}
            height={1410}
            className="absolute -bottom-[570px] -right-[200px]"
          />
        </div>
      </Container>
    </div>
  );
};
