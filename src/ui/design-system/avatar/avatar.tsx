import Image from "next/image";

interface Props {
  size?: "small" | "medium" | "large";
  src: string;
  alt: string;
}

export const Avatar = ({ size = "medium", src, alt }: Props) => {
  let sizeAvatar: number;

  switch (size) {
    case "small":
      sizeAvatar = 24;
      break;
    case "medium": //valeur par défaut
      sizeAvatar = 34;
      break;
    case "large":
      sizeAvatar = 50;
      break;
  }

  return (
    <div>
      <Image
        src={src ? src : "/assets/images/default-avatar.png"}
        alt={alt}
        width={sizeAvatar}
        height={sizeAvatar}
        className="rounded-full"
      />
    </div>
  );
};
