import clsx from "clsx";
import { Typography } from "../typography/typography";

interface Props {
  isLoading: boolean;
  placeholder: string;
  type?: "text" | "email" | "password";
  register: any;
  errors: any;
  errorMessage?: string;
  id: string;
  required?: boolean;
  isAutoCompleted?: boolean;
  label?: string;
}

export const Input = ({
  isLoading,
  placeholder,
  type = "text",
  register,
  errors,
  errorMessage = "ce champs doit être renseigné",
  id,
  required = true,
  isAutoCompleted = false,
  label,
}: Props) => {
  return (
    <div className="space-y-2">
      {label && (
        <Typography
          variant="caption4"
          component="div"
          theme={errors[id] ? "danger" : "gray"}
        >
          {label}
        </Typography>
      )}
      <input
        type={type}
        placeholder={placeholder}
        className={clsx(
          isLoading && "cursor-not-allowed",
          errors[id]
            ? " placeholder-alert-danger text-alert-danger"
            : " placeholder-gray-600",
          "w-full p-4 font-light border border-gray-400 rounded focus:outline-none focus:ring-1 focus:ring-primary"
        )}
        disabled={isLoading} //si le form est en chargement le champs est non clicable
        {...register(id, {
          required: {
            value: required,
            message: errorMessage,
          },
        })} //permet d'enregistrer les infos, on fait un spread operator et on met l'id de notre champs
        autoComplete={isAutoCompleted ? "on" : "off"}
      />
      {errors[id] && (
        <Typography variant="caption4" component="div" theme="danger">
          {errors[id]?.message}
        </Typography>
      )}
    </div>
  );
};
