import clsx from "clsx";
import { Spinner } from "./spinner";

interface Props {}

export const SpinnerFull = () => {
  return (
    <div className="flex items-center justify-center h-screen">
      <Spinner size="large" />
    </div>
  );
};
