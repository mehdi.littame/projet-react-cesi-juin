import { useState } from "react";
import { OnboardingView } from "./onboarding.view";
import { WelcomeStep } from "@/ui/modules/onboarding/components/steps/welcome-step/welcome-steps";
import { OnboardingStepsListInterface } from "@/types/onboarding-steps-list";
import { ProfileStep } from "@/ui/modules/onboarding/components/steps/profile-step/profile-step";

export const OnboardingContainer = () => {
  const [currentStep, setCurrentStep] = useState<number>(1);
  console.log("CURRENT STEP", currentStep);

  const StepLists: OnboardingStepsListInterface[] = [
    {
      id: 1,
      label: "Bienvenue",
      component: { step: WelcomeStep },
    },
    {
      id: 2,
      label: "Profil",
      component: { step: ProfileStep },
    },
    {
      id: 3,
      label: "Avatar",
      component: { step: ProfileStep },
    },
  ];

  const getCurrentStep = () => {
    return StepLists.find((el) => el.id === currentStep);
  };

  console.log("etape en cours", getCurrentStep());

  const next = () => {
    if (currentStep < StepLists.length) {
      setCurrentStep(currentStep + 1);
    }
  };

  const previous = () => {
    if (currentStep > 1) {
      setCurrentStep(currentStep - 1);
    }
  };

  const isFirstStep = () => {
    if (currentStep === 1) {
      return true;
    }
    return false;
  };

  const isFinalStep = () => {
    if (currentStep === StepLists.length) {
      return true;
    }
    return false;
  };

  return (
    <OnboardingView
      getCurrentStep={getCurrentStep}
      next={next}
      prev={previous}
      isFirstStep={isFirstStep}
      isFinalStep={isFinalStep}
      stepLists={StepLists}
    />
  );
};
