import { BaseComponentProps } from "@/types/onboarding-steps-list";
import { OnboardingFooter } from "../../footer/onboarding-footer";
import { Container } from "@/ui/components/container/container";
import { OnboardingTabs } from "../tabs/onboarding-tabs";
import { Typography } from "@/ui/design-system/typography/typography";
import Image from "next/image";
import { ProfilStepForm } from "./profile-step-form";
import { SubmitHandler, useForm } from "react-hook-form";
import { OnboardingProfilFielsType } from "@/types/forms";
import { UseToggle } from "@/hooks/use-toggle";
import { firestoreUpdateDocument } from "@/api/firestore";
import { useAuth } from "@/context/AuthUserContext";
import { toast } from "react-toastify";

export const ProfileStep = ({
  prev,
  next,
  isFirstStep,
  isFinalStep,
  stepLists,
  getCurrentStep,
}: BaseComponentProps) => {
  //hook Toggle pour freezer mes champs le temps du chargement
  const { value: isLoading, setValue: setLoading } = UseToggle();

  //on recup l'id user avec le hook useAuth
  const { authUser } = useAuth();
  //console.log("authUser", authUser);

  //toutes les fonctions de hookForm qu'on recupere (comme dans la partie creation user)
  const {
    handleSubmit,
    control,
    formState: { errors },
    register,
    reset,
    setValue,
  } = useForm<OnboardingProfilFielsType>();

  //recup les infos du form et les envoi a firestore via firestoreUpdateDoc
  const handleUpdateUserDocument = async (
    formData: OnboardingProfilFielsType
  ) => {
    const { error } = await firestoreUpdateDocument(
      "users",
      authUser.uid,
      formData
    );
    if (error) {
      setLoading(false);
      toast.error(error.message);
      return;
    }

    setLoading(false);
    reset();
    next(); //passer à l'etape suivante
  };

  //traiter les infos du formulaire
  const onSubmit: SubmitHandler<OnboardingProfilFielsType> = async (
    formData
  ) => {
    //declencher le loading
    setLoading(true);
    console.log("formData", formData);

    //on declenche ensuite la fonction
    handleUpdateUserDocument(formData);
  };

  return (
    <div className="relative h-screen pb-[91px]">
      <div className="h-full overflow-auto">
        <Container className="grid h-full grid-cols-12">
          <div className="relative z-10 flex items-center h-full col-span-6 py-10">
            <div className="w-full space-y-5 pb-4.5">
              <OnboardingTabs
                tabs={stepLists}
                getCurrentStep={getCurrentStep}
              />
              <Typography component="h1" variant="h1">
                Présente-toi
              </Typography>
              <Typography variant="body-base" component="p" theme="gray">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Phasellus sollicitudin nunc libero, ut dapibus diam tempor nec.
                Nunc id arcu convallis turpis ultrices vestibulum tristique non
                mi.
              </Typography>
            </div>
          </div>
          <div className="flex items-center h-full col-span-6">
            {/* formulaire a recuperer  */}
            <div className="flex justify-end w-full">
              {/* on lui passe un objet form avec les erreurs */}
              <ProfilStepForm
                form={{
                  errors,
                  control,
                  register,
                  handleSubmit,
                  onSubmit,
                  isLoading,
                }}
              />
            </div>
          </div>
        </Container>
      </div>
      <OnboardingFooter
        prev={prev}
        next={handleSubmit(onSubmit)}
        isFirstStep={isFirstStep}
        isFinalStep={isFinalStep}
        isLoading={isLoading}
      />
    </div>
  );
};
