import { FormsType } from "@/types/forms";
import { Input } from "@/ui/design-system/forms/input";
import { TextArea } from "@/ui/design-system/forms/text-area";

interface Props {
  form: FormsType;
}

export const ProfilStepForm = ({ form }: Props) => {
  //destructuring
  const { register, errors, isLoading } = form;

  return (
    <form className="w-full max-w-md space-y-4">
      <Input
        label="Pseudo"
        isLoading={isLoading}
        placeholder="Indique ton pseudo"
        type="text"
        register={register}
        errors={errors}
        errorMessage="renseigne un pseudo"
        //doit avoir le même nom que celui dans l'interface de forms
        id="displayName"
      />

      <Input
        label="Profession"
        isLoading={isLoading}
        placeholder="Dev front-end"
        type="text"
        register={register}
        errors={errors}
        errorMessage="renseigne une specialité"
        //doit avoir le même nom que celui dans l'interface de forms
        id="speciality"
      />

      <TextArea
        label="Bio"
        isLoading={isLoading}
        placeholder="Présente toi en quelques lignes..."
        rows={5}
        register={register}
        errors={errors}
        // errorMessage="renseigne une bio"
        //doit avoir le même nom que celui dans l'interface de forms
        id="biography"
        required={false}
      />
    </form>
  );
};
