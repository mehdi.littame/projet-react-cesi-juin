import { BaseComponentProps } from "@/types/onboarding-steps-list";
import { Container } from "@/ui/components/container/container";
import { Button } from "@/ui/design-system/button/button";
import { Typography } from "@/ui/design-system/typography/typography";
import { OnboardingFooter } from "@/ui/modules/onboarding/components/footer/onboarding-footer";
import Image from "next/image";
import { OnboardingTabs } from "../tabs/onboarding-tabs";

export const WelcomeStep = ({
  next,
  isFirstStep,
  isFinalStep,
  stepLists,
  getCurrentStep,
}: BaseComponentProps) => {
  return (
    <div className="relative h-screen">
      {/* integration page */}
      <div className="h-full overflow-auto">
        <Container className="grid h-full grid-cols-12">
          <div className="relative z-10 flex items-center h-full col-span-6 py-10">
            <div className="w-full space-y-5 pb-4.5">
              <OnboardingTabs
                tabs={stepLists}
                getCurrentStep={getCurrentStep}
              />
              <Typography component="h1" variant="h1">
                Bienvenue sur notre app !
              </Typography>
              <Typography variant="body-base" component="p" theme="gray">
                Lorem Ipsum sin dolor sit amet Lorem Ipsum sin dolor sit
                ametLorem Ipsum sin dolor sit ametLorem Ipsum sin dolor sit
                ametLorem Ipsum sin dolor sit ametLorem Ipsum sin dolor sit amet
              </Typography>
            </div>
          </div>
          <div className="flex items-center h-full col-span-6">
            <div className="w-full">
              <Image
                src="/assets/svg/rocket.svg"
                alt="Image d'illustration"
                width={800}
                height={800}
              />
            </div>
          </div>
        </Container>
      </div>

      <OnboardingFooter
        next={next}
        // prev={prev}
        isFirstStep={isFirstStep}
        isFinalStep={isFinalStep}
      />
    </div>
  );
};
