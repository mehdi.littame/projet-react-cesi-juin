import { BaseComponentProps } from "@/types/onboarding-steps-list";
import { OnboardingFooter } from "./components/footer/onboarding-footer";

export const OnboardingView = ({
  getCurrentStep,
  next,
  prev,
  isFirstStep,
  isFinalStep,
  stepLists,
}: BaseComponentProps) => {
  console.log(getCurrentStep);

  if (getCurrentStep()?.component) {
    const Component = getCurrentStep()?.component.step;
    return (
      <div>
        {Component && (
          <Component
            getCurrentStep={getCurrentStep}
            next={next}
            prev={prev}
            isFirstStep={isFirstStep}
            isFinalStep={isFinalStep}
            stepLists={stepLists}
          />
        )}
      </div>
    );
  }

  return null;
};
