import { CallToActionView } from "@/ui/design-system/call-to-action/call-to-action.view";
import { HighlightsView } from "./Highlights/highlights.view";
import { CurrentCourseView } from "./currentCourse/currentCourse.view";
import { FeaturedView } from "./featured/featured.view";
import { HeroTopView } from "./hero-top/hero-top.view"
import { JoinSlackView } from "./joinSlack/joinSlack.view";

export const LandingPageView = () => {
    return( 
    <>
        <HeroTopView/>
        <FeaturedView/>
        <JoinSlackView/>
        <CurrentCourseView/>
        <HighlightsView/>
        <CallToActionView/>
    </>
    );
};
