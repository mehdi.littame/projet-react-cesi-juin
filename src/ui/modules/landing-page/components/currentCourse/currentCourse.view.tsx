import { LinkTypes } from "@/lib/link-type";
import { Container } from "@/ui/components/container/container";
import { Button } from "@/ui/design-system/button/button";
import { Logo } from "@/ui/design-system/logo/logo";
import { Typography } from "@/ui/design-system/typography/typography";
import Image from "next/image";
import { RiPlayCircleLine } from "react-icons/ri";

export const CurrentCourseView = () => {
  return (
    <div className="bg-gray-400/60">
      <Container className="py-24 text-center">
        <Typography variant="h2" component="h2" className="mb-2.5">
          Formations au code !
        </Typography>
        <Typography variant="lead" component="h3" className="mb-5">
          Apprends à coder avec nous
        </Typography>
        <Typography variant="caption3" theme="gray" component="p" className="mb-16">
          Si tu veux un CV plus sexy que ton ex, suis cette formation complète !
        </Typography>
        <a href="#" target="_blank">
          <div className="relative bg-gray-500 rounded h-[626px]">
            <div className="flex flex-col items-center justify-center gap-2 relative h-full bg-gray z-10 rounded opacity-0 hover:opacity-85 text-white animate">
              <RiPlayCircleLine size={80}/>
              <Typography variant="caption1" theme="white" className="uppercase" weight="medium">
                Voir la formation
                </Typography>
            </div>
            <Image fill src="assets/images/lesson-reactjs.jpg" alt="formation React" className="object-cover object-center rounded" />
          </div>
        </a>
      </Container>
    </div>
  );
};
