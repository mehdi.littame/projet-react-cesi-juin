import { firebaseCreateUser, sendEmailToConfirm } from "@/api/authentication";
import { UseToggle } from "@/hooks/use-toggle";
import { RegisterFormFielsType } from "@/types/forms";
import { Button } from "@/ui/design-system/button/button";
import { Typography } from "@/ui/design-system/typography/typography";
import { SubmitHandler, useForm } from "react-hook-form";
import { toast } from "react-toastify";
import { RegisterView } from "./register.view";
import { firestoreCreateDocument } from "@/api/firestore";

export const RegisterContainer = () => {
  const {
    value: isLoading,
    setValue: setIsLoading,
    toggle,
  } = UseToggle({ initial: false }); //ici on renomme notre hook

  const {
    handleSubmit,
    control,
    formState: { errors }, // fait ref a un objet d'erreur
    register,
    setError, //permet de définir les erreurs ex longueur d'un password
    reset, //permet de rafraichir le formulaire (reset des champs)
  } = useForm<RegisterFormFielsType>();

  //fonction creer doc firestore
  const handleCreateUserDocument = async (
    collectionName: string,
    documentID: string,
    document: object
  ) => {
    const { error } = await firestoreCreateDocument(
      collectionName,
      documentID,
      document
    );
    if (error) {
      toast.error(error.message);
      setIsLoading(false);
      return;
    }
    toast.success("Bienvenue sur l'app!");
    setIsLoading(false); // en cas de success on arrete le loading
    reset(); //reinitialise directement le formulaire (hook de useForm)

    //plus tard envoyer l'email de confirmation
    //on appelle la fonction créée
    sendEmailToConfirm();
    toast.success("verifier votre boite mail");
  };

  const handleCreateUserAuthentication = async ({
    email,
    password,
    how_did_know_us,
  }: RegisterFormFielsType) => {
    const { error, data } = await firebaseCreateUser(email, password);
    if (error) {
      setIsLoading(false); // en cas d'erreur je passe le loading a false
      toast.error(error.message); //toast qui reprend le message d'error
      return;
    }

    //on creer notre document user avec ses propriétés
    const userDocumentData = {
      email: email,
      how_did_know_us: how_did_know_us,
      uid: data.uid,
      creation_date: new Date(),
    };
    //on appelle la fonction avec l'objet créé au dessus
    handleCreateUserDocument("users", data.uid, userDocumentData);
  };

  const onSubmit: SubmitHandler<RegisterFormFielsType> = async (formData) => {
    // //on recupere les infos transmises de ReactHookForm et on les consolelog
    setIsLoading(true); //on switch le state de isLoading à true
    console.log("formData", formData);
    const { email, password } = formData; //destructuring pour pas avoir a faire formData.email

    if (password.length <= 5) {
      setError("password", {
        type: "manual",
        message: "Le mot de passe doit comporter 6 caractères minimum",
      });
      setIsLoading(false);
      return; //le simple fait de return simple fait qu'on va pas aller plus loin (pas solliciter l'API firebase)
    }
    handleCreateUserAuthentication(formData);
  };

  return (
    <>
      {/* <Button action={toggle}>TEST du hook</Button> */}
      <Typography>{isLoading.toString()}</Typography>

      <RegisterView
        form={{
          errors,
          control,
          register,
          handleSubmit,
          onSubmit,
          isLoading,
        }}
      />
    </>
  );
};
