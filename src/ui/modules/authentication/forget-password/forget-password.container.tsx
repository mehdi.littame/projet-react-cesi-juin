import { ForgetPasswordFormFielsType } from "@/types/forms";
import { ForgetPasswordView } from "./forget-password.view";
import { useState } from "react";
import { SubmitHandler, useForm } from "react-hook-form";
import { UseToggle } from "@/hooks/use-toggle";
import { toast } from "react-toastify";
import { sendEmailToResetPassword } from "@/api/authentication";
import { useRouter } from "next/navigation";

export const ForgetPasswordContainer = () => {
  const router = useRouter();

  const {
    value: isLoading,
    setValue: setIsLoading,
    toggle,
  } = UseToggle({ initial: false }); //ici on renomme notre hook

  const {
    handleSubmit,
    control,
    formState: { errors }, // fait ref a un objet d'erreur
    register,
    setError, //permet de définir les erreurs ex longueur d'un password
    reset, //permet de rafraichir le formulaire (reset des champs)
  } = useForm<ForgetPasswordFormFielsType>();

  const handleResetPassword = async ({
    email,
  }: ForgetPasswordFormFielsType) => {
    const { error } = await sendEmailToResetPassword(email);
    if (error) {
      setIsLoading(false);
      toast.error(error.message);
      return;
    }
    toast.success(`Mail envoyé à ${email} - verifiez votre boite mail / spam`);
    setIsLoading(false);
    reset();
    router.push("/connexion");
  };

  const onSubmit: SubmitHandler<ForgetPasswordFormFielsType> = async (
    formData
  ) => {
    //on recupere les infos transmises de ReactHookForm et on les consolelog
    setIsLoading(true); //on switch le state de isLoading à true
    handleResetPassword(formData);
  };

  return (
    <ForgetPasswordView
      form={{
        errors,
        control,
        register,
        handleSubmit,
        onSubmit,
        isLoading,
      }}
    />
  );
};
