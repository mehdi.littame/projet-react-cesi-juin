import { FormsType } from "@/types/forms";
import { Button } from "@/ui/design-system/button/button";
import { Input } from "@/ui/design-system/forms/input";

interface Props {
    form: FormsType; //notre typage créé
  }

  export const LoginForm = ({ form }: Props) => {
    const { control, onSubmit, errors, isLoading, register, handleSubmit } = form;  
    return(
        <form onSubmit={handleSubmit(onSubmit)} className="pt-8 pb-5 space-y-4">
    <Input
    isLoading={isLoading}
    placeholder="johndoe@gmail.com"
    type = "email"
    register = {register}
    errors = {errors}
    errorMessage = "ce champs doit être renseigné"
    id="email" // on renseigne la meme valeur que dans notre objet
    />

<Input
    isLoading={isLoading}
    placeholder="Mot de passe"
    type = "password"
    register = {register}
    errors = {errors}
    errorMessage = "ce champs doit être renseigné"
    id="password" // on renseigne la meme valeur que dans notre objet
    />

      <Button isLoading= {isLoading} type="submit" fullWidth> Connexion</Button>
    </form>
    );
};