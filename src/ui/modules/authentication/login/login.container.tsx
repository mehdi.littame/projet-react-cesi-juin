import { LoginFormFielsType } from "@/types/forms";
import { useEffect, useState } from "react";
import { SubmitHandler, useForm } from "react-hook-form";
import { LoginView } from "./login.view";
import { getAuth, onAuthStateChanged } from "firebase/auth";
import { auth } from "@/config/firebase-config";
import { UseToggle } from "@/hooks/use-toggle";
import { firebaseSignInUser } from "@/api/authentication";
import { toast } from "react-toastify";
import { useRouter } from "next/router";
import { Button } from "@/ui/design-system/button/button";

export const LoginContainer = () => {
  const router = useRouter();
  const {
    value: isLoading,
    setValue: setIsLoading,
    toggle,
  } = UseToggle({ initial: false }); //ici on remplace le UseState par notre Hook useToggle

  // fonction firebase pour voir si utilisateur est connecté dans un useEffect
  useEffect(() => {
    onAuthStateChanged(auth, (user) => {
      if (user) {
        // Si on a un user on assigne uid
        const uid = user.uid;
        console.log("user", uid);

        // ...
      } else {
        console.log("pas connecté");
      }
    });
  }, []);

  const {
    handleSubmit,
    control,
    formState: { errors }, // fait ref a un objet d'erreur
    register,
    setError, //permet de définir les erreurs ex longueur d'un password
    reset, //permet de rafraichir le formulaire (reset des champs)
  } = useForm<LoginFormFielsType>();

  const handleSignInUser = async ({ email, password }: LoginFormFielsType) => {
    const { error } = await firebaseSignInUser(email, password);
    if (error) {
      setIsLoading(false);
      toast.error(error.message); //on recup le message de error pour l'afficher dans une fenetre de toast
      return;
    }
    toast.success("Bienvenue sur l'app !");
    setIsLoading(false);
    reset(); //rafraichir la vue du formulaire à l'initial
    router.push("/mon-espace"); //fonction push de router qui redirige
  };

  const onSubmit: SubmitHandler<LoginFormFielsType> = async (formData) => {
    //on recupere les infos transmises de ReactHookForm et on les consolelog
    setIsLoading(true); //on switch le state de isLoading à true

    //destructuring de password
    const { password } = formData;

    if (password.length <= 5) {
      setError("password", {
        type: "manuel",
        message: "Ton mot de passe doit comporter 6 carractères minimum",
      });
      setIsLoading(false);
      return;
    }
    handleSignInUser(formData);

    console.log("formData", formData);
  };
  return (
    <>
      <LoginView
        form={{
          errors,
          control,
          register,
          handleSubmit,
          onSubmit,
          isLoading,
        }}
      />
      <Button action={toggle}>TEST du hook</Button>
    </>
  );
};
