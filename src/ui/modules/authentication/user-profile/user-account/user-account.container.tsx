import { firebaseLogoutUser } from "@/api/authentication";
import { Button } from "@/ui/design-system/button/button";
import Router, { useRouter } from "next/router";
import { toast } from "react-toastify";

export const UserAccountContainer = () => {
  const handleLogOutUser = async () => {
    const { error } = await firebaseLogoutUser();

    if (error) {
      toast.error(error.message);
      return;
    }
    toast.success("À bientôt!");
    //Router.push("/");
  };

  return (
    <div className="flex justify-center pt-20 pb-40">
      <Button action={handleLogOutUser} variant="danger">
        Déconnexion
      </Button>
    </div>
  );
};
