import { useAuth } from "@/context/AuthUserContext";
import { GUEST, REGISTERED } from "@/lib/sessions-status";
import { sessionStatus } from "@/types/session-status";
import { SpinnerFull } from "@/ui/design-system/spinner/spinner-full";
import { useRouter } from "next/router";

interface Props {
  children: React.ReactNode;
  sessionStatus?: sessionStatus;
}
export const Session = ({ children, sessionStatus }: Props) => {
  const { authUserIsLoading, authUser } = useAuth();
  const router = useRouter();

  //recup dans authUser les infos de si onboarding a été complété
  const onboardingCompleted = authUser?.userDocument.onboardingCompleted;

  //condition pour rediriger vers Onboarding
  //pas de chargement des données user
  //utilisateur défini
  //onboarding pas complété dans la base
  //l'url en cours n'est pas déjà la page onboarding
  const RedirectToOnboarding = () => {
    return (
      !authUserIsLoading &&
      authUser &&
      !onboardingCompleted &&
      router.asPath !== "/onboarding"
    );
  };

  //si on ne doit pas rediriger vers onboarding
  const dontRedirectToOnboarding = () => {
    return (
      !authUserIsLoading &&
      authUser &&
      onboardingCompleted &&
      router.asPath === "/onboarding"
    );
  };

  //doit on rediriger?

  if (RedirectToOnboarding()) {
    router.push("/onboarding");
    //le temps de router on peut afficher un spinner
    return <SpinnerFull />;
  }

  //doit on ne pas rediriger?

  if (dontRedirectToOnboarding()) {
    router.push("/mon-espace");
    //le temps de router on peut afficher un spinner
    //return <SpinnerFull />;
  }

  //on test si on a une session de type registered
  // et si on a plus le chargement
  if (sessionStatus === REGISTERED && !authUserIsLoading) {
    //si j'ai un utilisateur j'affiche la vue
    if (authUser) {
      return <>{children}</>;
    }
    //sinon je redirige
    else {
      router.push("/connexion");
    }
  }

  //on test si on a une session de type guest
  // et si on a plus le chargement
  if (sessionStatus === GUEST && !authUserIsLoading) {
    //si j'ai un utilisateur j'affiche la vue
    if (!authUser) {
      return <>{children}</>;
    }
    //sinon je redirige
    else {
      router.push("/mon-espace");
    }
  }

  //si on est pas au chargement (si c'est false) + si on a pas de session utilisateur
  if (!sessionStatus && !authUserIsLoading) {
    return <>{children}</>;
  }
  //si ca load affichage du spinner en plein ecran
  return <SpinnerFull />;
};
