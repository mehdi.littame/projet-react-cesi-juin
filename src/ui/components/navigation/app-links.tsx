import { AppLinks } from "@/types/app-links";
import { RiDiscordFill, RiLinkedinFill, RiYoutubeFill } from "react-icons/ri";

export const footerApplicationLinks: AppLinks[] = [
    {
        label: "Accueil",
        baseUrl: "/",
        type: "internal"
    },
    {
        label: "Projets",
        baseUrl: "/projets",
        type: "internal"
    },
    {
        label: "Videos",
        baseUrl: "/videos",
        type: "internal"
    },
    {
        label: "Contact",
        baseUrl: "/contact",
        type: "internal"
    },
];

export const footerUsersLinks: AppLinks[] = [
    {
        label: "Mon espace",
        baseUrl: "/espace",
        type: "internal"
    },
    {
        label: "Connexion",
        baseUrl: "/connexion",
        type: "internal"
    },
    {
        label: "Inscription",
        baseUrl: "/connexion/inscription",
        type: "internal"
    },
    {
        label: "Mot de passe oublié",
        baseUrl: "/connexion/mot-de-passe-perdu",
        type: "internal"
    },
];

const footerInformationLinks: AppLinks[] = [
    {
        label: "CGU",
        baseUrl: "/CGU",
        type: "internal"
    },
    {
        label: "Confidentialité",
        baseUrl: "/confidentialité",
        type: "internal"
    },
    {
        label: "À propos",
        baseUrl: "/about",
        type: "internal"
    },
    {
        label: "Contact",
        baseUrl: "/contact",
        type: "internal"
    },
];

export const footerSocialsLinks: AppLinks[] = [
    {
        label: "Youtube",
        baseUrl: "https://youtube.com",
        type: "external",
        icon: RiYoutubeFill,
    },
    {
        label: "LinkedIn",
        baseUrl: "https://linkedin.com",
        type: "external",
        icon: RiLinkedinFill,
    },
    {
        label: "Discord",
        baseUrl: "https://discord.com",
        type: "external",
        icon : RiDiscordFill
    }
];


//tableau master
export const footerLinks = [
   {
    label: "App",
    links: footerApplicationLinks
   },
   {
    label: "Utilisateurs",
    links: footerUsersLinks
   }, 
   {
    label: "Informations",
    links: footerInformationLinks
   }, 
   {
   label: "Réseaux",
   links: footerSocialsLinks
  }
]