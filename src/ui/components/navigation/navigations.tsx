import { Children } from "react";
import { Container } from "../container/container";
import { Logo } from "@/ui/design-system/logo/logo";
import { Typography } from "@/ui/design-system/typography/typography";
import { Button } from "@/ui/design-system/button/button";
import Link from "next/link";
import DesignSystem from "@/pages/design-system";
import { ActiveLink } from "./active-link";
import { useAuth } from "@/context/AuthUserContext";
import { log } from "console";
import { AccountAvatarNavigation } from "./account-avatar-link";

interface Props {}

export const Navigation = ({}: Props) => {
  // on appelle authUser (les infos de l'utilisateur) via notre context useAuth et idem avec la notion de chargement
  const { authUser, authUserIsLoading } = useAuth();
  console.log("authUser", authUser);
  //console.log("authUserIsLoading", authUserIsLoading);

  //GERER LE MENU SELON LE SESSIONSTATUS
  const authentificationSystem = (
    <div className="flex items-center gap-2">
      <Button baseUrl="/connexion" size="small">
        Connexion
      </Button>
      <Button baseUrl="/connexion/inscription" size="small" variant="secondary">
        Rejoindre
      </Button>
    </div>
  );

  return (
    <div className="border-b-2 border-gray-400">
      <Container className="flex items-center justify-between py-1.5 gap-7">
        <Link href="/">
          <div className="flex items-center gap-2.5">
            <Logo size="small" />
            <div className="flex flex-col">
              <div className="text-gray font-extrabold text-[24px]">
                Mon site React
              </div>
              <Typography variant="caption4" theme="gray" component="span">
                Découvre mes projets pros et persos
              </Typography>
            </div>
          </div>
        </Link>

        <div className="flex items-center gap-7">
          <Typography
            variant="caption3"
            component="div"
            className="flex items-center gap-7"
          >
            <ActiveLink href="/design-system">Design System</ActiveLink>
            <ActiveLink href="/projets">Projet</ActiveLink>
            <ActiveLink href="/videos">Videos</ActiveLink>
            <ActiveLink href="/contact">Contact</ActiveLink>
          </Typography>

          {/* si je n'ai pas authuser (pas enregistrer) je renvoi ma balise 
          avec les btn standard authentificationSystem  */}
          {!authUser ? authentificationSystem : <AccountAvatarNavigation />}
        </div>
      </Container>
    </div>
  );
};
