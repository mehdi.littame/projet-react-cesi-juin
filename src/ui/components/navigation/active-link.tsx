import clsx from "clsx";
import Link from "next/link";
import { useRouter } from 'next/router'
import { useMemo } from "react";


interface Props {
    href : string;
    children : React.ReactNode;
}

export const ActiveLink =({href, children}: Props)=>{
    const router = useRouter()

    const isActive: boolean = useMemo(() =>{  //hook useMemo fonctionne comme une fct fléchée
        return router.pathname===href   //router.pathname est il égale à href ? O/N
    }, [router.pathname, href]) //on ajoute un tableau de dépendance qui contient le chemin et le href, si une des 2 infos changes, useMemo va se reactiver sinon on fait rien

    return(
        <Link href={href} className={clsx(isActive && "text-primary font-medium")}>
            {children}
        </Link> // si alors on applique la classname
    )
}