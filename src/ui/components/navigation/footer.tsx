import { Children } from "react";
import { Container } from "../container/container";
import { Logo } from "@/ui/design-system/logo/logo";
import { Typography } from "@/ui/design-system/typography/typography";
import { Button } from "@/ui/design-system/button/button";
import Link from "next/link";
import DesignSystem from "@/pages/design-system";
import { ActiveLink } from "./active-link";
import { FaLinkedin } from "react-icons/fa6";
import { footerApplicationLinks, footerLinks, footerUsersLinks } from "./app-links";
import { v4 as uuidv4 } from "uuid";
import { AppLinks, FooterLinks } from "@/types/app-links";
import { LinkTypes } from "@/lib/link-type";
import { SocialNetworksButtons } from "./social-networks-buttons";

interface Props {}

export const Footer = ({}: Props) => {
  const currentYear = new Date().getFullYear();

  const footerNavigationList = footerLinks.map((colomnLinks) => (
    <FooterLink key={uuidv4()} data={colomnLinks}/>
  ));

  return (
    <div className="bg-gray ">
      <Container className="flex justify-between pt-16">
        <div className="flex flex-col items-center gap-1">
          <Link href="/">
            <Logo size="medium" variant="white" />
          </Link>

          <Typography variant="caption1" theme="white" weight="medium">
            Formations gratuites
          </Typography>
          <Typography variant="caption3" theme="gray">
            Abonne toi à la chaine
          </Typography>
        </div>
        <div className="flex gap-7">
          {footerNavigationList}
        </div>
      </Container>
      <Container className="pt-9 pb-11 space-y-11">
        <hr className="text-gray-800" />
        <div className="flex items-center justify-between">
          <Typography variant="caption4" theme="gray">
            Copyright © {currentYear} |{" "}
            <a href="https://mehdilittame.com/" target="_blank">
              Mehdi Littamé{" "}
            </a>
          </Typography>
          <div className="">
            <SocialNetworksButtons  theme="gray" className="flex items-center gap-2.5"/>
          </div>
        </div>
      </Container>
    </div>
  );
};

interface footerLinkProps {
  data:FooterLinks;
}
const FooterLink =( { data }: footerLinkProps) =>{
    const linksList = data.links.map((link) => (
        <div key={uuidv4()}>
            {link.type=== LinkTypes.INTERNAL &&(
                <ActiveLink href={link.baseUrl}>{link.label}</ActiveLink>
            )}

            {link.type===LinkTypes.EXTERNAL &&(
            <a href={link.baseUrl} target="_blank">{link.label}</a>
            )}
        </div>
      ));

    return(
        <div className="min-w-[190px]">
            <Typography theme="white" variant="caption2" weight="medium" className="pb-5">
                {data.label}
            </Typography>
            <Typography theme="gray" variant="caption3" className="space-y-4">
                {linksList}           
            </Typography>
        </div>
    )
}