import { useAuth } from "@/context/AuthUserContext";
import { Avatar } from "@/ui/design-system/avatar/avatar";
import { Typography } from "@/ui/design-system/typography/typography";
import Link from "next/link";

export const AccountAvatarNavigation = () => {
  //on recupere les infos de l'user
  const { authUser } = useAuth();
  const { photoURL, displayName } = authUser;

  return (
    <Link href="/mon-espace" className="flex items-center gap-2">
      <Avatar
        src={photoURL}
        alt={displayName ? displayName : "avatar"}
        size="large"
      />
      <div className="max-w-[160px]">
        <Typography variant="caption2" weight="medium" className="truncate">
          {displayName ? displayName : "Nom du compte"}
        </Typography>
        <Typography variant="caption4" weight="medium" theme="gray">
          Mon compte
        </Typography>
      </div>
    </Link>
  );
};
