import { Typography } from "@/ui/design-system/typography/typography";
import clsx from "clsx";
import { useRouter } from "next/router";
import { RiHome3Line } from "react-icons/ri";
import { v4 as uuidv4 } from "uuid";
import { Container } from "../container/container";
import Link from "next/link";

export const Breadcrumbs = () => {
  const router = useRouter(); //le hook useRouter de NextJS nous donne plein d'attribut sur l'url en cours
  const asPath = router.asPath; //on stock le asPath dans une variable
  const segments = asPath.split("/"); // on la split selon les / avec chaque valeur dans un tableau
  const lastSegment = segments[segments.length - 1]; // on prend la derniere valeur du tableau
  segments[0] = "Accueil";

  console.log("lastsegments", lastSegment);

  const view = segments.map((path, index) => (
    <div key={uuidv4()}>
      <Link href={index > 0 ? `/${segments.slice(1, index +1).join("/")}` : "/"}>
        <Typography
          variant="caption3"
          component="span"
          className={clsx(
            path !== lastSegment ? "text-gray-600" : "text-gray",
            "capitalize hover:text-gray animate"
          )}
        >
          {path !== "Accueil" ? (
            path.replace(/-/g, " ")
          ) : (
            <RiHome3Line className="inline -mt-1" />
          )}
        </Typography>
        {path !== lastSegment && (
          <Typography
            variant="caption2"
            component="span"
            className="ml-2 text-gray-600"
          >
            /
          </Typography>
        )}
      </Link>
    </div>
  ));

  return <Container className="flex items-center gap-2 py-7">{view}</Container>;
};
