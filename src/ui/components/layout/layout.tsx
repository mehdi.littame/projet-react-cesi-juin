import { Breadcrumbs } from "../breadcrumbs/breadcrumbs";
import { Footer } from "../navigation/footer";
import { Navigation } from "../navigation/navigations";
import { Container } from "../container/container";
import { UserNavigation } from "../navigation/user-navigation";
import { Session } from "../session/session";
import { sessionStatus } from "@/types/session-status";

interface Props {
  children: React.ReactNode;
  isDisplayBreadcrumbs?: boolean;
  sidebar?: boolean;
  sessionStatus?: sessionStatus;
}

export const Layout = ({
  children,
  isDisplayBreadcrumbs = true,
  sidebar = false,
  sessionStatus,
}: Props) => {
  let view: React.ReactElement = <></>;

  if (sidebar) {
    view = (
      <Container className="mb-14">
        <div className="grid grid-cols-12 gap-7">
          <div className="col-span-3">
            <UserNavigation />
          </div>
          <div className="col-span-9">{children}</div>
        </div>
      </Container>
    );
  } else {
    view = <>{children}</>;
  }

  return (
    <Session sessionStatus={sessionStatus}>
      <Navigation />
      {isDisplayBreadcrumbs && <Breadcrumbs />}
      {view}
      <Footer />
    </Session>
  );
};
