import { GUEST } from "@/lib/sessions-status";
import { Layout } from "@/ui/components/layout/layout";
import { Seo } from "@/ui/components/seo/seo";
import { LoginContainer } from "@/ui/modules/authentication/login/login.container";

export default function Connexion() {
  return (
    <>
      <Seo title="Connexion" description="Connexion à mon site" />
      <Layout sessionStatus={GUEST}>
        <LoginContainer />
      </Layout>
    </>
  );
}
