import { REGISTERED } from "@/lib/sessions-status";

import { Layout } from "@/ui/components/layout/layout";

import { Seo } from "@/ui/components/seo/seo";
import { Session } from "@/ui/components/session/session";

import { Typography } from "@/ui/design-system/typography/typography";

import { UserAccountContainer } from "@/ui/modules/authentication/user-profile/user-account/user-account.container";
import { OnboardingContainer } from "@/ui/modules/onboarding/onboarding.container";

export default function Onboarding() {
  return (
    <>
      <Seo title="Onboarding" description="Onboarding" />
      <Session sessionStatus={REGISTERED}>
        <OnboardingContainer />
      </Session>
    </>
  );
}
