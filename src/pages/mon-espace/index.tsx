import { REGISTERED } from "@/lib/sessions-status";
import { Layout } from "@/ui/components/layout/layout";
import { Seo } from "@/ui/components/seo/seo";
import { UserAccountContainer } from "@/ui/modules/authentication/user-profile/user-account/user-account.container";

export default function Connexion() {
  return (
    <>
      <Seo title="Mon espace" description="mon compte" />
      {/* On defini que ce layout doit concerner les membre enregistrés */}
      <Layout sidebar sessionStatus={REGISTERED}>
        <UserAccountContainer />
      </Layout>
    </>
  );
}
