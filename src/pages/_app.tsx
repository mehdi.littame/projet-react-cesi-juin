import { AuthUserProvider } from "@/context/AuthUserContext";
import "@/styles/globals.css";
import type { AppProps } from "next/app";
//librairie Toastify
import { Flip, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

export default function App({ Component, pageProps }: AppProps) {
  return (
    <AuthUserProvider>
      <ToastContainer
        position="top-center" //position de la fenetre
        autoClose={8000} //8 secondes avant de disparaitre
        transition={Flip} //type d'animation
      />
      <Component {...pageProps} />
    </AuthUserProvider>
  );
}
