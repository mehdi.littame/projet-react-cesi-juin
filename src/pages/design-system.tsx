//COMPONENTS
import { Navigation } from "@/ui/components/navigation/navigations";
import { Container } from "@/ui/components/container/container";
import { Seo } from "@/ui/components/seo/seo";

//DESIGN SYSTEM
import { Avatar } from "@/ui/design-system/avatar/avatar";
import { Button } from "@/ui/design-system/button/button";
import { Logo } from "@/ui/design-system/logo/logo";
import { Spinner } from "@/ui/design-system/spinner/spinner";
import { Typography } from "@/ui/design-system/typography/typography";

//ICONES
import { RiUser6Fill } from "react-icons/ri";
import { Layout } from "@/ui/components/layout/layout";

export default function DesignSystem() {
  return (
    <>
      <Seo title="Design system" description="Page de design system de mon application" />
      <Layout>
        <Container className="py-10 space-y-10">
        {/*composant typography*/}
        <div className="space-y-2">
          <Typography variant="caption2" weight="medium">
            Typography
          </Typography>
          <div className="flex flex-col gap-2 p-5 border border-gray-400 divide-y-2 divide-gray-400 rounded">
            <div className="pb-5 space-y-2">
              <Typography variant="caption3" weight="medium">
                Display
              </Typography>
              <Typography variant="display" weight="medium">
                Nothing is impossible
              </Typography>
            </div>

            <div className="py-5 space-y-2">
              <Typography variant="caption3" weight="medium">
                H1
              </Typography>
              <Typography variant="h1" weight="medium">
                Nothing is impossible
              </Typography>
            </div>

            <div className="py-5 space-y-2">
              <Typography variant="caption3" weight="medium">
                H2
              </Typography>
              <Typography variant="h2" weight="medium">
                Nothing is impossible
              </Typography>
            </div>

            <div className="py-5 space-y-2">
              <Typography variant="caption3" weight="medium">
                H3
              </Typography>
              <Typography variant="h3" weight="medium">
                Nothing is impossible
              </Typography>
            </div>

            <div className="py-5 space-y-2">
              <Typography variant="caption3" weight="medium">
                H4
              </Typography>
              <Typography variant="h4" weight="medium">
                Daily Report: Removing Checks to the Power of the Internet
                Titans
              </Typography>
            </div>

            <div className="py-5 space-y-2">
              <Typography variant="caption3" weight="medium">
                H5
              </Typography>
              <Typography variant="h5" weight="medium">
                Daily Report: Removing Checks to the Power of the Internet
                Titans
              </Typography>
            </div>
          </div>
        </div>

        <div className="flex items-start gap-7">
          {/*composant spinner*/}
          <div className="space-y-2">
            <Typography variant="caption2" weight="medium">
              Spinners
            </Typography>
            <div className="flex items-center gap-2 p-5 border border-x-gray-400 rounded">
              <Spinner size="small" />
              <Spinner />
              <Spinner size="large" />
            </div>
          </div>
        </div>
        {/*composant Avatar*/}
        <div className="space-y-2">
          <Typography variant="caption2" weight="medium">
            Avatar
          </Typography>
          <div className="flex items-center gap-2 p-5 border border-x-gray-400 rounded">
            <Avatar
              size="small"
              src="/assets/images/avatar.png"
              alt="avatar en small"
            />
            <Avatar src="/assets/images/avatar.png" alt="avatar en standard" />
            <Avatar
              size="large"
              src="/assets/images/avatar.png"
              alt="avatar en large"
            />
          </div>
        </div>

        {/*composant Logo*/}
        <div className="space-y-2">
          <Typography variant="caption2" weight="medium">
            Logo
          </Typography>
          <div className="flex items-center gap-2 p-5 border border-x-gray-400 rounded">
            <Logo size="very-small" variant="primary" />
            <Logo size="small" variant="primary" />
            <Logo />
            <Logo size="large" variant="primary" />
          </div>
          <div className="flex items-center gap-2 p-5 border border-x-gray-400 rounded bg-[#CD213A]">
            <Logo size="very-small" variant="white" />
            <Logo size="small" variant="white" />
            <Logo variant="white" />
            <Logo size="large" variant="white" />
          </div>
        </div>

        <div className="max-w-6xl mx-auto space-y-5 py-10">
          {/*composant buttons*/}
          <div className="space-y-2">
            <Typography variant="caption2" weight="medium">
              Buttons
            </Typography>
            <div className="flex flex-col gap-2 p-5 border border-gray-400 divide-y-2 divide-gray-400 rounded">
              <div className="flex items-center gap-4 p-10">
                <Button isLoading size="small">
                  Accent
                </Button>
                <Button
                  isLoading
                  size="small"
                  icon={{ icon: RiUser6Fill }}
                  iconPosition="left"
                >
                  Accent
                </Button>
                <Button
                  isLoading
                  size="small"
                  icon={{ icon: RiUser6Fill }}
                  iconPosition="right"
                >
                  Accent
                </Button>
                <Button isLoading variant="secondary" size="small">
                  Secondary
                </Button>
                <Button isLoading variant="outline" size="small">
                  Outline
                </Button>
                <Button isLoading variant="disabled" size="small" disabled>
                  Disabled
                </Button>
                <Button isLoading variant="ico" icon={{ icon: RiUser6Fill }} />
              </div>

              <div className="flex items-center gap-4 p-10">
                <Button size="small">Accent</Button>
                <Button
                  size="small"
                  icon={{ icon: RiUser6Fill }}
                  iconPosition="left"
                >
                  Accent
                </Button>
                <Button
                  size="small"
                  icon={{ icon: RiUser6Fill }}
                  iconPosition="right"
                >
                  Accent
                </Button>
                <Button variant="secondary" size="small">
                  Secondary
                </Button>
                <Button variant="outline" size="small">
                  Outline
                </Button>
                <Button variant="disabled" size="small" disabled>
                  Disabled
                </Button>
                <Button variant="ico" icon={{ icon: RiUser6Fill }} />
              </div>

              <div className="flex items-center gap-4 p-10">
                <Button
                  variant="ico"
                  size="small"
                  icon={{ icon: RiUser6Fill }}
                  iconTheme="secondary"
                />
                <Button variant="ico" icon={{ icon: RiUser6Fill }} />
                <Button
                  variant="ico"
                  size="large"
                  icon={{ icon: RiUser6Fill }}
                  iconTheme="gray"
                />

                <Button
                  size="small"
                  icon={{ icon: RiUser6Fill }}
                  iconPosition="left"
                >
                  Accent
                </Button>

                <Button size="small">Accent</Button>
                <Button variant="secondary" size="small">
                  Secondary
                </Button>
                <Button variant="outline" size="small">
                  Outline
                </Button>
                <Button variant="disabled" size="small" disabled>
                  Disabled
                </Button>
              </div>
              <div className="flex items-center gap-4 p-10">
                <Button>Accent</Button>
                <Button variant="secondary">Secondary</Button>
                <Button variant="outline">Outline</Button>
                <Button variant="disabled" disabled>
                  Disabled
                </Button>
              </div>

              <div className="flex items-center gap-4 p-10">
                <Button size="large">Accent</Button>
                <Button variant="secondary" size="large">
                  Secondary
                </Button>
                <Button variant="outline" size="large">
                  Outline
                </Button>
                <Button variant="disabled" size="large" disabled>
                  Disabled
                </Button>
              </div>
            </div>
          </div>
        </div>
      </Container>
      </Layout>
      
    </>
  );
}
