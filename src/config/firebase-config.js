// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getFirestore } from "firebase/firestore";

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyCbbesvuXN8VYhE8eer-2KorTp_uUCfli4",
  authDomain: "sitereact-e5bef.firebaseapp.com",
  projectId: "sitereact-e5bef",
  storageBucket: "sitereact-e5bef.appspot.com",
  messagingSenderId: "514749138210",
  appId: "1:514749138210:web:527e23dbc43ef9e092917b",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

// Initialize Firebase Authentication and get a reference to the service
export const auth = getAuth(app);

//initialisation firestore
export const db = getFirestore(app);
