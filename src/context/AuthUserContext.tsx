import useFirebaseAuth from "@/hooks/use-firebase-auth";
import { UserDocument } from "@/types/user";
import { createContext, useContext } from "react";

//on reprend tous les elements de user et on les initiales (a vide)
const init = {
  uid: "",
  displayName: "",
  emailVerified: false,
  phoneNumber: "",
  photoURL: "",
  userDocument: {} as UserDocument,
};

const authUserContext = createContext({
  //les elements de notre hook, defini avec une valeur initiale
  authUser: init,
  authUserIsLoading: true,
});

interface Props {
  children: React.ReactNode;
}

//va englober toute notre app, on lui passe un element enfant
export function AuthUserProvider({ children }: Props) {
  //on recupere le hook qu'on a créé
  const auth = useFirebaseAuth();

  // on retourne les valeurs recup pour les transferer
  return (
    <authUserContext.Provider
      value={{
        authUser: auth.authUser as {
          uid: string;
          email: string;
          displayName: string;
          emailVerified: boolean;
          phoneNumber: string;
          photoURL: string;
          userDocument: UserDocument;
        },
        authUserIsLoading: auth.authUserIsLoading,
      }}
    >
      {children}
    </authUserContext.Provider>
  );
}

//exporter notre hook pour l'utiliser le context dans l'app
export const useAuth = () => useContext(authUserContext);
