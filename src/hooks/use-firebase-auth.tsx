import { auth, db } from "@/config/firebase-config";
import { UserDocument, UserInterface } from "@/types/user";
import { User, onAuthStateChanged } from "firebase/auth";
import { doc, onSnapshot } from "firebase/firestore";
import { useEffect, useState } from "react";

export default function useFirebaseAuth() {
  //valeur par defaut des utilisateur à null dans ce State
  const [authUser, setAuthUser] = useState<UserInterface | null>(null); // il faut mettre les 2 dans le cas ou l'utilisateur n'est pas connecté
  const [authUserIsLoading, setauthUserIsLoading] = useState<boolean>(true); //est ce qu'on est en train de charger

  const formatAuthUser = (user: UserInterface) => ({
    uid: user.uid,
    email: user.email,
    displayName: user.displayName,
    emailVerified: user.emailVerified,
    phoneNumber: user.phoneNumber,
    photoURL: user.photoURL,
  });

  const getUserDocument = async (user: UserInterface) => {
    if (auth.currentUser) {
      //fonction onSnapchot Doc firestore qui prend en param la bdd, la collection ,et le document (ici je l'isole dans une variable document ref juste avant)
      const documentRef = doc(db, "users", auth.currentUser.uid);
      //user a qui on va assigner les propriétés de doc par la suite
      const compactUser = user;
      onSnapshot(documentRef, async (doc) => {
        //on verifie si le document existe si c'est le cas, on rajoute les données de l'utilisateur
        if (doc.exists()) {
          compactUser.userDocument = doc.data() as UserDocument;
        }
        //mettre à jour les données de l'user (authUser) en prenant en compte les infos avant la mise à jour
        // et donc redeclencher le state (on les fusionne)
        setAuthUser((prevAuthUser) => ({
          //ancien etat du state
          ...prevAuthUser,
          ...compactUser,
        }));
        //on arrete ensuite de charer
        setauthUserIsLoading(false);
      });
    }
  };

  const AuthStateChanged = async (authState: UserInterface | User | null) => {
    //si pas d'authentification- d'utilisateur
    if (!authState) {
      setAuthUser(null);
      setauthUserIsLoading(false);
      return;
    }
    //on a un utilisateur
    setauthUserIsLoading(true);
    const formattedUser = formatAuthUser(authState);
    //on lui passe toutes les infos formattées
    await getUserDocument(formattedUser);
  };

  //reprend la fonction pour voir si notre utilisateur est connecté
  useEffect(() => {
    const unsubscribe = onAuthStateChanged(auth, AuthStateChanged);
    return () => unsubscribe();
  }, []);

  return {
    authUser,
    authUserIsLoading,
  };
}
