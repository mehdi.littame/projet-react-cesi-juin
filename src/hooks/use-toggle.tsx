import { useState } from "react";


interface Props {
    initial?: boolean;
}

export const UseToggle =({initial = false}: Props= {}) =>{

 const [value, setValue] = useState<boolean>(initial); // state on lui donne un type et une valeure par défaut a initiale

//notre fonction toggle precedement dans container
const toggle = () =>{
    setValue(!value)
};

    return {
        value,
        setValue,
        toggle
    }
}