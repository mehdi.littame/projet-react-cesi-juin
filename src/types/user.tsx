import { Timestamp } from "firebase/firestore";

export interface UserInterface {
  uid: string;
  email: string | null;
  displayName: string | null;
  emailVerified: boolean;
  phoneNumber: string | null;
  photoURL: string | null;
  //la notion de document des données qui appartient à la base
  userDocument?: UserDocument;
}

// on reprend les champs qu'on a pour l'instant dans la base
export interface UserDocument {
  uid: string;
  email: string;
  how_did_know_us: string;
  creation_date: Timestamp;
  onboardingCompleted: boolean;
  //...
}
