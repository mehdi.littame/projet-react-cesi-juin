export interface BaseComponentProps {
  next: () => void;
  prev: () => void;
  isFirstStep: () => boolean;
  isFinalStep: () => boolean;
  getCurrentStep: () => OnboardingStepsListInterface | undefined;
  stepLists: OnboardingStepsListInterface[];
}

export interface OnboardingStepsListInterface {
  id: number;
  label: string;
  component: {
    step: React.ComponentType<BaseComponentProps>;
  };
}
