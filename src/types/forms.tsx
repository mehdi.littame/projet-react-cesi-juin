export interface FormsType {
  control: any;
  onSubmit: any;
  errors: any;
  isLoading: boolean; //qui nous permettra de gérer le chargement des données plus tard
  register: any;
  handleSubmit: any;
}

export interface RegisterFormFielsType {
  email: string;
  password: string;
  how_did_know_us: string;
}

export interface LoginFormFielsType {
  email: string;
  password: string;
}

export interface ForgetPasswordFormFielsType {
  email: string;
}

//typage du formulaire de l'onBoarding
export interface OnboardingProfilFielsType {
  displayName: string;
  speciality: string;
  biography: string;
}
